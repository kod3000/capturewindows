from PIL import ImageGrab
import partial
import time
import os
import os, os.path

print("\n\n\n\t\t[ Launching Windows Apps ]\n\n")
# we are also going to tell this app to run the regualar programs that are used inside the day
# add the path to the programs that you want to run with this app
# os.system("start C:\Users\Dany\AppData\Local\WhatsApp\WhatsApp.exe")


def captureDesktop(save_imaged):
    img = ImageGrab.grab(all_screens=True)
    img.save(save_imaged)


# App Variables
chosenRootFolderName = "Pro" # root folder name ( this where all the day to day folders will be creaeted )
frequency = 10 # how often the app will run ( in seconds )
frameRate = 10 # this is good number but the lower the slower the video will play.

rootFolder = "C:\\"+chosenRootFolderName+"\\"
os.chdir(r"C:\\")
# print(str(os. getcwd()))
try: 
# first up create folder ..
# if the folder already exist then use it
    os.mkdir(chosenRootFolderName) 
except OSError as error: 
    print("\nApp Setup Note : \nRoot Directory already existed so no need to create it..\n")

folder_Date = time.strftime("%Y-%m-%d")
actualTime = time.strftime("%Y-%m-%d-%H-%M-%S")
# establish the days folder to be used..
myPath = rootFolder+folder_Date+"\\"
try: 
    os.mkdir(myPath) 
except OSError as error: 
    print("App Setup Note : \nTodays working directory already existed so no need to create it\nContinuing forward..")
checkFolder = os.listdir(myPath)
# print (str(len(checkFolder))+" files in folder")
starting_number = len(checkFolder)

print("\n\n\n\t\t[ ScreenCapture App started ]")
print("\t\t[ "+time.strftime("%H:%M:%S")+" ]")
if starting_number == 0:
    print("\t\t[ Starting New Day ]")
else:
    print("\t\t[ Continuing Day ]")
    print("\t\t[ "+str(starting_number)+" screen captures already existing for today ]")
    print("\n\n\nTo generate a video for this day, use the following command:")
    print("\nffmpeg -framerate "+str(frameRate)+" -i "+myPath+"%d-screenshot.png -c:v libx264 -r 30 -pix_fmt yuv420p "+rootFolder+folder_Date+".avi\n\n\n\n")

if starting_number == 0:
    # lets go ahead and take our first picture and not even wait..
    img_string = myPath+str(starting_number)+'-screenshot.png'
    print("\t\t[ Creating First Screenshot of the day ]")
    print("\t\t"+img_string)
    captureDesktop(img_string)
    starting_number = starting_number+1
    print("\t\t[ Screenshot Successfully taken ]")


# perform loop every minute
while True:

    # we are going to take a picture every minute
    # you can change this to whatever you want..
    # its based on seconds 60 seconds = 1 minute
    time.sleep(frequency)
    img_string = myPath+str(starting_number)+'-screenshot.png'
    print("\t\t[ Taking Screenshot ]")
    captureDesktop(img_string)
    print("\t\t[ Screenshot Successfully taken ]")
    print("\t\t[ "+time.strftime("%H:%M:%S")+" ]")
    starting_number = starting_number+1
