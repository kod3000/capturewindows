![demo output](https://bitbucket.org/kod3000/capturewindows/raw/dd5f3b0f1c2551cb04eb975d3a665404874de38d/imgs/output.gif)

## Description :
----
**The purpose of this program is to record and log the days production via screenshots of the desktop. 
Based on a timer, screen captures are taken and saved to a folder (each new day creates its own folder for that day). From there, using ffmpeg, a video can be composited
using the screenshots for that day. This program is designed to be run as a cron job at some point. But we still aren't there yet..**

**BONUS - lines 7-10 give instructions on how to have the app open default apps for you at startup**

## App Variables :
----
Change to your liking ;)

### chosenRootFolderName (String)

This folder is default created in C drive and will hold all the daily folders.

###  frequency (int)

This is the timer variable and will dictate how often the app will run in seconds. 
If you're super motivated.. lower this to about 20 seconds to have a nice playing video at the end.

## Running :

-----

			python capture.py
			

## Install/Setup Depenancies :
----
### Download and Install Python2 (restart might be needed after install.)

Link :
 		https://www.python.org/ftp/python/2.7.18/python-2.7.18.amd64.msi


### Using Powershell Install pip and other needed libraries



		python -m pip install --upgrade --force-reinstall pip
		python -m pip install urllib3
		python -m pip install partial
		python -m pip install pillow

### Next Download and Install FFMPEG

Link : 
		https://github.com/BtbN/FFmpeg-Builds/releases/download/latest/ffmpeg-master-latest-win64-gpl-shared.zip

	
**Unzip ffmpeg and move the root folder that holds "bin, doc, etc.' into the c: drive and name it ffmpeg.** 

**Using powershell make a system Alias for ffmpeg**

			Set-Alias -Name ffmpeg -Value C:\ffmpeg\bin\ffmpeg.exe
			

## Dev Notes

		query user  Dany